#ifndef __eloquent__RestWriterAttach__
#define __eloquent__RestWriterAttach__

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <string>
#include <vector>

// Internal
#include "GmailWriterFactory.h"

// Library Initialization
extern "C" void* Attach( void );

#endif /* defined(__eloquent__RestWriterAttach__) */
