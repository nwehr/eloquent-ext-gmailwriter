//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// C++
#include <string>
#include <mutex>

// Boost
#include <boost/property_tree/ptree.hpp>

// Internal
#include "Eloquent/Logging.h"
#include "GmailWriter.h"

namespace Eloquent {
	static const int DEFAULT_BUFFER_SIZE = 4096;
}

///////////////////////////////////////////////////////////////////////////////
// RestWriter : IOExtension
///////////////////////////////////////////////////////////////////////////////
Eloquent::GmailWriter::GmailWriter( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_LogMutex
								 , streamlog::severity_log& i_Log
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , int& i_NumWriters )
: IOExtension( i_Config, i_LogMutex, i_Log, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters )
, m_URI( 0 )
, m_Session( 0 )
, m_Request( 0 )
{
	{
		std::unique_lock<std::mutex> LogLock( m_LogMutex );
		m_Log( Eloquent::LogSeverity::SEV_INFO ) << "GmailWriter::GmailWriter() - info - setting up a writer for " << std::endl;
	}
	
	m_URI = new Poco::URI();
	m_URI->setHost( m_Config.second.get<std::string>( "host" ) );
	m_URI->setPort( m_Config.second.get<int>( "port" ) );
	
	m_Session = new Poco::Net::HTTPClientSession( m_URI->getHost(), m_URI->getPort() );
	
	m_Request = new Poco::Net::HTTPRequest( Poco::Net::HTTPRequest::HTTP_POST, m_URI->getPathAndQuery(), Poco::Net::HTTPMessage::HTTP_1_1 );
	
	m_Request->setContentType( "text/plain" );
	m_Request->add( "Accept", "text/plain" );
	m_Request->set("User-Agent", "Eloquent RestWriter/1.0");
	m_Session->setKeepAlive(true);
	
}

Eloquent::GmailWriter::~GmailWriter() {
	std::unique_lock<std::mutex> LogLock( m_LogMutex );
	
	m_Log( Eloquent::LogSeverity::SEV_INFO )
		<< " - RestWriter::~RestWriter()() - info - shutting down a writer for "
		<< m_URI->getPathAndQuery()
		<< std::endl;
	
	delete m_Request;
	delete m_URI;
	delete m_Session;

}

void Eloquent::GmailWriter::Write( const std::string& i_Data ) {
	try {
		m_Request->setContentLength( i_Data.size() );
		
		std::cout << m_URI->getPathAndQuery() << std::endl;
		
		std::ostream& ostr = m_Session->sendRequest( *m_Request ) << i_Data;
		
		if( ostr.bad() || ostr.fail() ) {
			std::cout << "network or server failure" << std::endl;
		}
		
	} catch ( Poco::Exception& e ) {
		std::unique_lock<std::mutex> LogLock( m_LogMutex );
		m_Log( Eloquent::LogSeverity::SEV_ERROR ) << " - RestWriter::Write() - error - " << e.message() << std::endl;
		
	}
	
}

void Eloquent::GmailWriter::operator()() {
	boost::optional<int> Batch = m_Config.second.get_optional<int>( "batch" );
	
	std::string BatchData	= "";
	int			BatchCount	= 0;
	
	while( true ){
		try {
			QueueItem& Item = NextQueueItem();
			
			{
				if( Batch.is_initialized() ) {
					BatchData.append( m_FilterCoordinator->FilterData( Item.Data() ) );
					
					if( *Batch == (BatchCount + 1)  ) {
						std::unique_lock<std::mutex> QueueLock( m_QueueMutex );
						
						Write( BatchData );
						
						BatchData	= "";
						BatchCount	= 0;
						
					} else ++BatchCount;
					
				} else {
					std::unique_lock<std::mutex> QueueLock( m_QueueMutex );
					Write( m_FilterCoordinator->FilterData( Item.Data() ) );
					
				}
				
			}
			
			PopQueueItem();

		} catch( const std::exception& e ) {
			std::unique_lock<std::mutex> LogLock( m_LogMutex );
			m_Log( Eloquent::LogSeverity::SEV_ERROR ) << " - RestWriter::operator()() - error - " << e.what() << std::endl;
			
		} catch( ... ) {
			std::unique_lock<std::mutex> LogLock( m_LogMutex );
			m_Log( Eloquent::LogSeverity::SEV_ERROR ) << " - RestWriter::operator()() - error - UNKNOWN RUNTIME ERROR" << std::endl;
			
		}
		
	}
			
	delete this;
	
	return; 
	
}
