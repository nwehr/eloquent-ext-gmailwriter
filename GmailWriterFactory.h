#ifndef __eloquent__RestWriterFactory__
#define __eloquent__RestWriterFactory__

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <string>
#include <vector>

// Boost
#include <boost/smart_ptr.hpp>
#include <boost/property_tree/ptree.hpp>

// Internal
#include "Eloquent/Extensions/Factories/IOExtensionFactory.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// RestWriterFactory : IOExtensionFactory
	///////////////////////////////////////////////////////////////////////////////
	class GmailWriterFactory : public IOExtensionFactory {
		GmailWriterFactory();
	public:
		GmailWriterFactory();
		virtual ~GmailWriterFactory();
		
		virtual IOExtension* New( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_LogMutex
								 , streamlog::severity_log& i_Log
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , int& i_NumWriters );
		
	};
	
}

#endif /* defined(__eloquent__RestWriterFactory__) */


(<?Aa> some_regex)